const { mockFetchHelper } = require("./mock_api.js");
const fs = require('fs');

let rawdata = fs.readFileSync('albums.json');
let albums = JSON.parse(rawdata);

(async()=>{
    let data = await mockFetchHelper(true, albums, 1000);

    data.albums.forEach((entry)=>{
        console.log(`${entry.band_name}, ${entry.album_title}, ${entry.genres}, ${entry.last_listened}, ${entry.release_date}`)
    });
})();
